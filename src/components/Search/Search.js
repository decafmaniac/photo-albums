import React, { useState, useEffect } from 'react';
import { Input, Container } from 'semantic-ui-react';
import axios from 'axios';
import Gallery from "react-photo-gallery";
import {v4 as uuid} from 'uuid';
import { Storage }  from 'aws-amplify';

const Search = () => {
    const [images, setImages] = useState([] );
    const [inputText, setInputText] = useState('');
    const [query, setQuery] = useState('redux');

    useEffect(() => {
        const fetchData = async () => {
          const result = await axios(
            `https://pixabay.com/api/?key=4731759-454f23b6b2515702080462d44&q=${query}&image_type=photo`,
          );
    
          setImages(result.data.hits);
        };
    
        fetchData();
      }, [query]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const fileName = uuid();
        axios({method: 'GET', url: 'https://pixabay.com/get/57e3d7434253ac14f6da8c7dda793379163ed9e05a4c704c702773d59648c358_640.jpg', responseType: 'blob'}).then(response => {
            if(response) {
                const file = new Blob([response.data], {type: 'image/png'})
                console.log('asdasda ', file)
                Storage.put(
                    fileName, 
                    file, 
                    {
                      customPrefix: { public: 'uploads/' },
                      metadata: { albumid: '47a9cff1-d789-4a8d-b474-1318f0b21543' }
                    }
                  );
                return file;
            }
        })
    
        setQuery(inputText);
    }

   
    

    const sortedImages = images.map(p => ({ src: p.webformatURL, 
                                            width: p.webformatWidth, 
                                            height: p.webformatHeight}));
    
    return (
        <Container>
            <Input fluid  
            placeholder='Search...' 
            action={{ content: 'Search', onClick: handleSubmit }}
            onChange={e => setInputText(e.target.value)} 
            value={inputText}/>

            {images.length > 0 ? (
                 <Gallery photos={sortedImages}></Gallery>
            ) : null}


            
        </Container>
    )
} 

export default Search;