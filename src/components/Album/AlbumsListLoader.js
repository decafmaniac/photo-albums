import React from 'react';
import { Connect } from 'aws-amplify-react';
import { graphqlOperation}  from 'aws-amplify';
import AlbumsList from './AlbumList';

// 4. NEW: Add a new string to query all albums
const ListAlbums = `query ListAlbums {
    listAlbums(limit: 9999) {
        items {
            id
            name
        }
    }
}`;

const SubscribeToNewAlbums = `
  subscription OnCreateAlbum {
    onCreateAlbum {
      id
      name
    }
  }
`;

// 5. NEW: Add an AlbumsListLoader component that will use the 
//    Connect component from Amplify to provide data to AlbumsList

// 2. EDIT: Update AlbumsListLoader to work with subscriptions
class AlbumsListLoader extends React.Component {

    // 2a. NEW: add a onNewAlbum() function 
    // for handling subscription events
    onNewAlbum = (prevQuery, newData) => {
        // When we get data about a new album, 
        // we need to put in into an object 
        // with the same shape as the original query results, 
        // but with the new data added as well
        let updatedQuery = Object.assign({}, prevQuery);
        updatedQuery.listAlbums.items = prevQuery.listAlbums.items.concat([newData.onCreateAlbum]);
        return updatedQuery;
    }
  
    render() {
        return (
            <Connect 
                query={graphqlOperation(ListAlbums)}
                
                // 2b. NEW: Listen to our 
                // SubscribeToNewAlbums subscription
                subscription={graphqlOperation(SubscribeToNewAlbums)} 
  
                // 2c. NEW: Handle new subscription messages
                onSubscriptionMsg={this.onNewAlbum}
            >
                
                {({ data, loading, errors }) => {
                    if (loading) { return <div>Loading...</div>; }
                    if (errors.length > 0) { return <div>{JSON.stringify(errors)}</div>; }
                    if (!data.listAlbums) return;
  
                return <AlbumsList albums={data.listAlbums.items} />;
                }}
            </Connect>
        );
    }
  }

  export default AlbumsListLoader;