import React from 'react';
import { Header, Segment } from 'semantic-ui-react';
import S3ImageUpload from '../Photos/S3ImageUpload';
import PhotosList from '../Photos/PhotosList';

const AlbumDetails = (props) => {
    return (
      <Segment>
        <Header as='h3'>{props.album.name}</Header>
        <S3ImageUpload albumId={props.album.id}/>        
        <PhotosList photos={props.album.photos.items} />
      </Segment>
    )
}

export default AlbumDetails;