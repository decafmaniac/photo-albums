import React, { useState } from 'react';
import { Header, Input, Segment } from 'semantic-ui-react';
import { API, graphqlOperation}  from 'aws-amplify';


const NewAlbum = () => {
    const [albumName, setAlbumName] = useState('');
  
    const handleSubmit = async (event) => {
      event.preventDefault();
      const NewAlbum = `mutation NewAlbum($name: String!) {
        createAlbum(input: {name: $name}) {
          id
          name
        }
      }`;
      
      const result = await API.graphql(graphqlOperation(NewAlbum, { name: albumName }));
      console.info(`Created album with id ${result.data.createAlbum.id}`);
    }
  
    return (
      <Segment>
          <Header as='h3'>Add a new album</Header>
           <Input
            type='text'
            placeholder='New Album Name'
            icon='plus'
            iconPosition='left'
            action={{ content: 'Create', onClick: handleSubmit }}
            name='albumName'
            value={albumName}
            onChange={event => {
              setAlbumName(event.target.value)
            }}
           />
          </Segment>
    )
  }

  export default NewAlbum;