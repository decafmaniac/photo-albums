import React from 'react';
import { Header, List, Segment } from 'semantic-ui-react';
import makeComparator from '../../utils/sortComparator';
import { NavLink } from 'react-router-dom';

// 3. NEW: Add an AlbumsList component for rendering 
//    a sorted list of album names
const AlbumsList = (props) => {
    return (
      <Segment>
        <Header as='h3'>My Albums</Header>
        <List divided relaxed>
          {props.albums.sort(makeComparator('name')).map(album =>
            <List.Item key={album.id}>
              <NavLink  to={`/albums/${album.id}`}>{album.name}</NavLink>
            </List.Item>
          )}
        </List>
      </Segment>
    );
}

export default AlbumsList;