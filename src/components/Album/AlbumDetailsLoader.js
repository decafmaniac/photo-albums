import React from 'react';
import { Connect } from 'aws-amplify-react';
import { graphqlOperation}  from 'aws-amplify';
import AlbumDetails from '../Album/AlbumDetails';

const GetAlbum = `query GetAlbum($id: ID!) {
    getAlbum(id: $id) {
      id
      name
      photos {
        items {
          thumbnail {
            width
            height
            key
          }
        }
        nextToken
      }
    }
  }
  `;

// 3. NEW: Create an AlbumDetailsLoader component
//    to load the details for an album
const AlbumDetailsLoader = (props) => {
    return (
      <Connect query={graphqlOperation(GetAlbum, { id: props.id })}>
        {({ data, loading, errors }) => {
          if (loading) { return <div>Loading...</div>; }
          if (errors.length > 0) { return <div>{JSON.stringify(errors)}</div>; }
          if (!data.getAlbum) return;
          return <AlbumDetails album={data.getAlbum} />;
        }}
      </Connect>
    );
}

export default AlbumDetailsLoader;