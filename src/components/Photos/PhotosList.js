import React from 'react';
import { Divider } from 'semantic-ui-react';
import { S3Image } from 'aws-amplify-react';

const PhotosList = (props) => {
    return (
      <div>
        <Divider hidden />
        {props.photos.map(photo =>
        <S3Image 
          key={photo.thumbnail.key} 
          imgKey={photo.thumbnail.key.replace('public/', '')} 
          style={{display: 'inline-block', 'paddingRight': '5px'}}
        />
      )}
      </div>
    );
  }

  export default PhotosList;