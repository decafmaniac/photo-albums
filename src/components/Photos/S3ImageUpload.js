import React, { useState } from 'react';
import { Form } from 'semantic-ui-react';
import {v4 as uuid} from 'uuid';
import { Storage }  from 'aws-amplify';


const S3ImageUpload = (props) => {

    const [uploading, setUploading] = useState(false);
  
    const onChange = async (e) => {
      const file = e.target.files[0];
      const fileName = uuid();
      setUploading(true);
      const result = await Storage.put(
        fileName, 
        file, 
        {
          customPrefix: { public: 'uploads/' },
          metadata: { albumid: props.albumId }
        }
      );
      console.log('Uploaded file: ', result);
      setUploading(false);
    }
      return (
        <div>
          <Form.Button
            onClick={() => document.getElementById('add-image-file-input').click()}
            disabled={uploading}
            icon='file image outline'
            content={uploading ? 'Uploading...' : 'Add Image' }
          />
          <input
            id='add-image-file-input'
            type="file"
            accept='image/*'
            onChange={onChange}
            style={{ display: 'none' }}
          />
        </div>
      );
  }

  export default S3ImageUpload;