import React from 'react';
import Amplify  from 'aws-amplify';
import aws_exports from './aws-exports';
import { withAuthenticator } from 'aws-amplify-react';
import { Grid } from 'semantic-ui-react';
import {BrowserRouter as Router, Route, NavLink} from 'react-router-dom';
import NewAlbum from './components/Album/NewAlbum';
import AlbumDetailsLoader from './components/Album/AlbumDetailsLoader';
import AlbumsListLoader from './components/Album/AlbumsListLoader';
import Search from './components/Search/Search'
import './App.css';

Amplify.configure(aws_exports);

const App = () => {

    return (
      <Router>
        <Grid columns={2} relaxed='very'>
          <Grid.Column>
            <Route path="/" exact component={NewAlbum}/>
            <Route path="/" exact component={AlbumsListLoader}/>
            <Route
              path="/albums/:albumId"
              render={ () => <div><NavLink to='/'>Back to Albums list</NavLink></div> }
            />
            <Route
              path="/albums/:albumId"
              render={ props => <AlbumDetailsLoader id={props.match.params.albumId}/> }
            />
          </Grid.Column>
          <Grid.Column>
            <Search />
          </Grid.Column>
        </Grid>
      </Router>
    );
  
}
export default withAuthenticator(App, {includeGreetings: true});
